kwantSpectrum |release| documentation
=====================================

.. toctree::
   :maxdepth: 2
   :numbered: 3

   about
   tutorial
   reference
